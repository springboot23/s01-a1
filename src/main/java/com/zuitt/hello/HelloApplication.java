package com.zuitt.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloApplication.class, args);
	}
//	// http://localhost:8080/api/hello
//	@GetMapping("api/hello")
//	public String hello() {
//		return String.format("Hello, world! Welcome to Java/Spring Boot web development.");
//	}

	// http://localhost:8080/api/hello?name=<name>
	@GetMapping("api/hello")
	public String hello(@RequestParam(value="name" , defaultValue = "world") String name) {
		return String.format("Hello, %s! Welcome to Java + Spring Boot web development.", name);
	}

	// http://localhost:8080/api/friend?name=<name>&age=<age>
	@GetMapping("api/friend")
	public String friend(@RequestParam(value = "name") String name, @RequestParam(value = "age") String age) {
		return String.format("Hello, my name is %s! I am %s years old.", name, age);
	}

	// http://localhost:8080/api/your-name/<name>
	@GetMapping("api/your-name/{name}")
	public String yourName(@PathVariable("name") String name) {
		return String.format("Hello, %s! How are you doing today?", name);
	}

	// http://localhost:8080/api/hi/name=<name>
	@GetMapping("api/hi/{name}")
	public String hi(@PathVariable("name") String name) {
		return String.format("%s" ,name);
	}

	// http://localhost:8080/api/course?name=<name>&course=<course>
	@GetMapping("api/course")
	public String course(@RequestParam(value = "name") String name,@RequestParam(value = "course") String course) {
		return String.format("%s,%s" ,name ,course);
	}
}
